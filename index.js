require('dotenv').config();

const express = require('express');
const app = express();
const port = process.env.PORT || 56201;

app.use(express.json());
app.use(express.text());

app.post('/square', (req,res)=>{

    const num = +req.body;

    console.log(num)
    if (isNaN(num)) {
        return res.status(400).send({error:"Not number"})
    }

    const result = num*num;

    res.send({
        number : num,
        square: result
    });
});

app.post('/reverse', (req,res)=>{

    const text = req.body
    const result = text.split("").reverse().join("")

    res.send(
       result);
});

function isLeapYear(year)
{
    return (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0);
}

app.get('/date/:year/:month/:day', (req, res)=> {

    const year = +req.params.year
    const month = +req.params.month
    const day = +req.params.day

    console.log(year)
    console.log(month)
    console.log(day)


    if (year < 0 || month < 0 || month >12 || day <0  || day > 31) {
        console.log("first")
        return res.status(400).send({error:"Invalid date"})
    }

    // if (month == 1 || month == 1 || month == 3 || month == 5 || month == 7 || month == 10 ||month == 12 ){
    //     if (day > 31) {
    //         return res.status(400).send({error:"Invalid data"})
    //     }
    // }

    if (month == 4 || month == 6 || month == 9 || month== 11){
        if (day > 30){
            console.log("second")
            return res.status(400).send({error:"Invalid date"})
        }
    }

    if (month == 2 ){
        if (isLeapYear(year) ){
            if (day > 29){
                console.log("third")
                return res.status(400).send({error:"Invalid date"})

            }
        }
        else {
            if (day > 28){
                return res.status(400).send({error:"Invalid data"})

            }
        }
    }

    //date
    const myDate = new Date(year,(month-1), day);
    const d = new Date();
    let dif = Math.floor((d - myDate)/(1000*60*60*24))

    if (dif < 0) {
        dif = Math.abs(dif)
    }

    res.send ({
        weekDay : myDate.toLocaleDateString('en-US',{weekday:'long'}),
        isLeapYear : isLeapYear(year),
        difference : dif
    })
});

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});